/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server ;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitol
 */
public class Server extends Thread{
    private final int serverPort;
    
    private ArrayList<ServerSlave> slaveList= new ArrayList<>();
    public ArrayList<Socket> listening_users=  new ArrayList<>();
    
    public Server(int serverPort)
    {
        this.serverPort = serverPort;
    }

    @Override
    public void run() {//listening for connection and calling slave object when getting one 
        try
        {
            ServerSocket serverSocket = new ServerSocket(serverPort);
            while (true)
            {
                System.out.println("About to accept connection........");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Accepted connection"+clientSocket);
                
                ServerSlave slave = new ServerSlave( clientSocket,this);
                slaveList.add(slave);
                slave.start();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public List<ServerSlave> get_slave_List() 
    {
        return slaveList;
    }
    
    
}
