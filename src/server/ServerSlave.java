package server;

import java.io.*;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
                                    
public class ServerSlave extends Thread {
    private final Socket clientSocket;
    private String login  =null;
    private Server server;
    
    private Connection con;
    private Statement st;
    private ResultSet rs;
    
    private String msg;
    
    
    InputStream inputStream =  null;
    OutputStream outputStream= null;
    public ServerSlave(Socket clientSocket,Server server) {
        this.clientSocket = clientSocket;
        this.server = server;
    }

    @Override
    public void run()
    {
        try
        {
            handle_client_socket();
        } catch (IOException | InterruptedException e)
        {
        } catch (SQLException ex) {
            Logger.getLogger(ServerSlave.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void handle_client_socket() throws IOException, InterruptedException, SQLException {
        inputStream =  clientSocket.getInputStream();
        outputStream= clientSocket.getOutputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        
        while((line = reader.readLine()) != null)//reads commands sent from client
        {
            String[] tokens = StringUtils.split(line);
            String cmd ;
            
            if(tokens != null && tokens.length > 0)
            {
                cmd = tokens[0];
            
                if("quit".equalsIgnoreCase(cmd))
                {
                    System.out.println("user quit");
                    break;
                }
                else if("login".equalsIgnoreCase(cmd))
                {
                    handle_login(outputStream, tokens);
                }
                else if("change_email".equalsIgnoreCase(cmd))
                {
                    change_email(tokens);
                }
                else if("member_of_this_group".equalsIgnoreCase(cmd))
                {
                    member_of_this_group(tokens);
                }
                else if("add_friend_to_group".equalsIgnoreCase(cmd))
                {
                    add_friend_to_group(tokens);
                }
                else if("change_email".equalsIgnoreCase(cmd))
                {
                    change_email(tokens);
                }
                else if("unfriend_friend".equalsIgnoreCase(cmd))
                {
                    unfriend_friend(tokens);
                }
                else if("user_complaint".equalsIgnoreCase(cmd))
                {
                    user_complaint(line);
                }
                else if("remove_member".equalsIgnoreCase(cmd))
                {
                    remove_member(tokens);
                }
                else if("get_add_friend_member_list".equalsIgnoreCase(cmd))
                {
                    get_add_friend_member_list(tokens);
                }
                else if("delete_user".equalsIgnoreCase(cmd))
                {
                    delete_user(tokens);
                }
                else if("get_pending_invite".equalsIgnoreCase(cmd))
                {
                    msg = get_pending_invite(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("get_user_list".equalsIgnoreCase(cmd))
                {
                    msg = get_user_list();
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("block_unblock_user".equalsIgnoreCase(cmd))
                {
                    msg = block_unblock_user(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("update_password_from_ad".equalsIgnoreCase(cmd))
                {
                    update_password_from_ad(tokens);
                    
                }
                else if("leave_group".equalsIgnoreCase(cmd))
                {
                    leave_group(tokens);
                    
                }
                else if("get_friend_list".equalsIgnoreCase(cmd))
                {
                    msg = get_friend_list(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("change_display_name".equalsIgnoreCase(cmd))
                {
                    change_display_name(tokens);
                }
                else if("search_user".equalsIgnoreCase(cmd))
                {
                    msg = search_user(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("accept_friend".equalsIgnoreCase(cmd))
                {
                    accept_friend(tokens);
                }
                else if("deny_friend".equalsIgnoreCase(cmd))
                {
                    deny_friend(tokens);
                }
                else if("change_password".equalsIgnoreCase(cmd))
                {
                    change_password(tokens);
                }  
                else if("add_friend".equalsIgnoreCase(cmd))
                {
                    if(add_friends(tokens))
                    {
                        msg=  "ok\n";
                    }
                    else
                    {
                        msg = "fail\n";
                    }
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }  
                else if("search_friends".equalsIgnoreCase(cmd))
                {
                    msg = search_friends(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }  
                else if("change_basic".equalsIgnoreCase(cmd))
                {
                    change_basic(tokens);
                }
                else if("check_duplicate_title_p_group".equalsIgnoreCase(cmd))
                {
                    if(!check_if_public_group_title_is_taken(line))
                    {
                        logg_new_public_group();
                    }
                   
                }
                else if("create_new_group".equalsIgnoreCase(cmd))
                {
                    logg_new_group(line);
                }
                else if("add_me".equalsIgnoreCase(cmd))
                {
                    server.listening_users.add(clientSocket);
                }
                else if("is_duplicate".equalsIgnoreCase(cmd))
                {
                    is_duplicate(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("log".equalsIgnoreCase(cmd))
                {
                    log(line);
                }
                else if("get_group_info".equalsIgnoreCase(cmd))
                {
                    msg = get_group_info(tokens);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                else if("get_user_log".equalsIgnoreCase(cmd))
                {
                    get_user_log(tokens);
                }
                else if("register_user".equalsIgnoreCase(cmd))
                {
                    register_user(tokens);
                }
                else if("register_user_ad".equalsIgnoreCase(cmd))
                {
                    register_user_ad(tokens);
                }
                else if("msg".equalsIgnoreCase(cmd))
                {
                    new_msg(line);
                    update();
                }
                else if("delete_group".equalsIgnoreCase(cmd))
                {
                    delete_group(tokens);
                }
                else if("join".equalsIgnoreCase(cmd))
                {
                    join_public_group(tokens);

                }
                else if("stop_thread".equalsIgnoreCase(cmd))
                {
                    msg = "stop\n";
                    
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                     
                    for(int x =0;x < server.listening_users.size();x++)
                    {
                        if(server.listening_users.get(x).equals(clientSocket))
                            server.listening_users.remove(x);
                    }

                }
                else if("desc".equalsIgnoreCase(cmd))
                {
                    msg = get_public_group_desc(tokens);
                    
                    outputStream.write(msg.getBytes());
                    outputStream.flush();

                }
                else if("get_previous_messages".equalsIgnoreCase(cmd))
                {
                    ArrayList<String> prev_msg = new ArrayList<>();
                    prev_msg= get_previous_messages(tokens);
                    send_prev_msg(prev_msg);
                }
                else if("member_of".equalsIgnoreCase(cmd))
                {
                    msg = get_member_of_group(tokens);
                    
                    outputStream.write(msg.getBytes());
                    outputStream.flush();

                }
                else if("not_a_member_of_public_group".equalsIgnoreCase(cmd))
                {
                    ArrayList<Integer> id_list = new ArrayList<>();
                    ArrayList<String> title_list = new ArrayList<>();
                     
                    id_list = is_a_member_of(tokens);
                    
                    title_list = check_public_groups_available(id_list);
                    
                    msg = get_response_string(title_list);
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
                
                else
                {
                    msg = "unknown " +cmd + "\n";
                    outputStream.write(msg.getBytes());
                    outputStream.flush();
                }
            }
            
            
        }
        
        clientSocket.close();
    }

   
    private void handle_login(OutputStream outputStream,String[] tokens) throws IOException, SQLException
    {
        if(tokens.length == 3)
        {
            String login = tokens[1];
            String password = tokens[2];
            if(user_auth(login,password))
            {
                this.login  = login;
                System.out.println("User auth:"+login);
                            
                outputStream.write(msg.getBytes());
            }
            else
            {
                System.out.println("User failed auth");
                            
                outputStream.write(msg.getBytes());
            }
                 
        }
    }
    public String getLogin() {
        return login; 
    }
    
    private Statement connection()//makes the connection to db
    {
        try {
            String url="jdbc:sqlite:villa.db";
            con = DriverManager.getConnection(url);
            st = (Statement)con.createStatement();
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(ServerSlave.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        return st;
    }

    private boolean user_auth(String login, String password) throws SQLException, IOException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select * from User_acc where username = ?");
        pstmt.setString(1, login);
        rs = pstmt.executeQuery();
        while (rs.next())
        {
            if(rs.getString(4).equals(login)&& rs.getString(6).equals(password))
            {
                msg = "ok"+" "+rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5)+" "+rs.getString(6)+" "+rs.getString(7)+" "+rs.getInt(8)+" "+rs.getInt(9)+"\n";
                rs.close();
                pstmt.close();
                return true;
            }
            else
            {
                rs.close();
                pstmt.close();
                msg = "nok\n";
                return false;
            }
        }
        rs.close();
        pstmt.close();
        msg = "nok\n";
        con.close();
        return false;
    }

    private void log(String line) throws IOException, SQLException {
        connection();
        String[] tokens = StringUtils.split(line,null,4);
        
        int id = Integer.parseInt(tokens[1]);
        int date = Integer.parseInt(tokens[2]);
        String txt = tokens[3]; 
        System.out.println(tokens[3]);
        
        PreparedStatement pstmt = con.prepareStatement("Insert into Log(userId,Text,Date) Values(?,?,?)");
         pstmt.setInt(1, id);
         pstmt.setString(2, txt);
         pstmt.setInt(3, date);
         pstmt.executeUpdate();
         pstmt.close();
         con.close();
         
    }

    private boolean  check_if_public_group_title_is_taken(String line) throws IOException, SQLException {
        connection();
        String[] tokens = StringUtils.split(line,null,3);
        PreparedStatement pstmt = con.prepareStatement("Select Title from Groups where Title = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        
        while (rs.next())
        {
            if(rs.getString(1).equals(tokens[1]))
            {
                rs.close();
                pstmt.close();
                msg = "nok\n";
                outputStream.write(msg.getBytes());
                outputStream.flush();
                con.close();
                return true;
            }
            else
            {
                rs.close();
                pstmt.close();
                msg = "ok\n";
                outputStream.write(msg.getBytes());
                outputStream.flush();
                con.close();
                return false;
            }
        }
        rs.close();
        pstmt.close();
        msg = "ok\n";
        outputStream.write(msg.getBytes());
        outputStream.flush();
        con.close();
        return false;
    }

    private void logg_new_public_group() throws IOException, SQLException {
        connection();
        BufferedReader readers = new BufferedReader(new InputStreamReader(inputStream));
        String[] tokens= StringUtils.split(readers.readLine(),null,4);    
        
        PreparedStatement pstmt = con.prepareStatement("Insert into Groups(Title,Date,creator,Desc,ispublic) Values(?,?,?,?,1)");

        pstmt.setString(1,tokens[0]);     
        pstmt.setInt(2,Integer.parseInt(tokens[1]));
        pstmt.setInt(3,Integer.parseInt(tokens[2])); 
        pstmt.setString(4,tokens[3]);

        pstmt.executeUpdate();

        rs = st.executeQuery("Select MAX(id) from Groups");

        pstmt = con.prepareStatement("Insert into Chat_members(user_id,chat_id) Values(?,?)");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        pstmt.setInt(2,rs.getInt(1)); 

        pstmt.executeUpdate();

        pstmt.close();
        rs.close();
        con.close();
        
    }

    private void logg_new_group(String line) throws IOException, SQLException {
        connection();
        String[] tokens= StringUtils.split(line,null,5);    
       
        PreparedStatement pstmt = con.prepareStatement("Insert into Groups(Title,Date,creator,Desc,ispublic) Values(?,?,?,?,0)");

        pstmt.setString(1,tokens[1]);     
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        pstmt.setInt(3,Integer.parseInt(tokens[3])); 
        pstmt.setString(4,tokens[4]);
        
        pstmt.executeUpdate();
        
        rs = st.executeQuery("Select MAX(id) from Groups");

        pstmt = con.prepareStatement("Insert into Chat_members(user_id,chat_id) Values(?,?)");
        pstmt.setInt(1,Integer.parseInt(tokens[3]));
        pstmt.setInt(2,rs.getInt(1)); 

        pstmt.executeUpdate();
        
        rs.close();
        pstmt.close(); 
        con.close();
    }

    private ArrayList is_a_member_of(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select chat_id from chat_members where user_id = ?");
        pstmt.setInt(1, Integer.parseInt(tokens[1]));
        
        ArrayList<Integer> chat_id_list = new ArrayList<>();
        rs = pstmt.executeQuery();
        while(rs.next())
        {
            chat_id_list.add(rs.getInt(1));
        }
        rs.close();
        pstmt.close();
        con.close();
        return chat_id_list;
    }

    private ArrayList<String> check_public_groups_available(ArrayList<Integer> id_list) throws SQLException {
        connection();
        ArrayList<String> title_list = new ArrayList<>();
        rs = st.executeQuery("Select id,title from groups where ispublic = 1");
        while(rs.next())
        {
            if(id_list.contains(rs.getInt(1)))
            {
                continue;
            }
            else
            {
                title_list.add(rs.getString(2));
            }
                
        }
        con.close();
        return title_list;
    }

    private String get_response_string(ArrayList<String> title_list) 
    {
        
        String response ="";
        for(String title:title_list)
        {
            response = response +title+" ";
        }
        response = response+"\n";
        
        return response;
    }

    private void join_public_group(String[] tokens) throws SQLException {
        connection();
        
        PreparedStatement pstmt = con.prepareStatement("Select id from groups where title = ? and ispublic = 1");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        
        pstmt = con.prepareStatement("Insert into Chat_members(user_id,chat_id) Values(?,?)");
        pstmt.setInt(1, Integer.parseInt(tokens[2]));
        pstmt.setInt(2, rs.getInt(1));
        
        pstmt.execute();
        
        rs.close();
        pstmt.close();
        con.close();
    }

    private String get_public_group_desc(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select desc from groups where title = ? and ispublic = 1");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        String temp = rs.getString(1);
        
        rs.close();
        con.close();
        return temp+"\n";
    }

    private String get_member_of_group(String[] tokens) throws SQLException {
        ArrayList<Integer> id_list = new ArrayList<>();
        id_list = is_a_member_of(tokens);
        String tilte = getTheMemberList(id_list);
        return tilte;
    }

    private String getTheMemberList(ArrayList<Integer> id_list) throws SQLException {
        connection();
        rs = st.executeQuery("Select id,title from groups ORDER BY Date DESC");
        String response = "";
        while(rs.next())
        {
            if(id_list.contains(rs.getInt(1)))
            {
                response +=rs.getString(2)+" ";
            }             
        }
        response +="\n";
        rs.close();
        con.close();
        return response;
    }

    private ArrayList<String> get_previous_messages(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select user_id,message from chat_mes where chat_id = ? Order by date ASC");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        rs = pstmt.executeQuery();
        
        ArrayList<Integer> idList = new ArrayList<>();
        ArrayList<String> messageList = new ArrayList<>();
        ArrayList<String> displayNameList = new ArrayList<>();
        ArrayList<String> msg = new ArrayList<>();
        
        int y= 0;
        while (rs.next())
        {
          idList.add(rs.getInt(1));
          messageList.add(rs.getString(2));
           y++;
        }
        for(int x = 0; x < y;x++)
        {
            pstmt = con.prepareStatement("Select display_name from user_acc where id = ?");
            pstmt.setInt(1,idList.get(x));
            rs = pstmt.executeQuery();;
            displayNameList.add(rs.getString(1));
        }
        
        for(int x = 0; x < y;x++)
        {
            msg.add(displayNameList.get(x)+":"+messageList.get(x)+"\n");
        }
        pstmt.close();
        rs.close();
        con.close();
        return msg;
         
    }

    private void send_prev_msg(ArrayList<String> prev_msg) throws IOException {
        
        for(String msges:prev_msg)
        {
            outputStream.write(msges.getBytes());
            outputStream.flush();
        }
            msg = "end\n";
            outputStream.write(msg.getBytes());
            outputStream.flush();
    }

    private void new_msg(String line) throws SQLException {
        connection();
        String[] tokens= StringUtils.split(line,null,5);  
        PreparedStatement pstmt = con.prepareStatement("Insert into Chat_mes(chat_id,user_id,date,message) Values(?,?,?,?)");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        pstmt.setInt(3,Integer.parseInt(tokens[3]));
        pstmt.setString(4,tokens[4]);
        pstmt.executeUpdate();
        
        pstmt = con.prepareStatement("Update groups set date =? where ID = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[3]));
        pstmt.setInt(2,Integer.parseInt(tokens[1]));
        
        pstmt.executeUpdate();
        
        pstmt.close();
        con.close();
    }

    private void update() throws IOException {
        msg = "update\n";
        
        for(Socket user: server.listening_users)
        {
           OutputStream serverOut= user.getOutputStream();
           serverOut.write(msg.getBytes());
           serverOut.flush();
        }
    }

    private void change_email(String[] tokens) throws SQLException, IOException {
        connection();
        
        PreparedStatement pstmt = con.prepareStatement("Select email from user_acc where email = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        while(rs.next())
        {
            if(tokens[1].equalsIgnoreCase(rs.getString(1)))
            {
                msg = "exists\n";
                outputStream.write(msg.getBytes());
                outputStream.flush();
                return;
            }
        }
        rs.close();
        pstmt.close();
        con.close();
        msg = "ok\n";
        outputStream.write(msg.getBytes());
        outputStream.flush();
        update_email(tokens);
        
    }

    private void update_email(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Update user_acc Set email=? where username=?");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.execute();
        
        pstmt.close();
        con.close();
    }

    private void change_display_name(String[] tokens) throws SQLException, IOException {
        connection();
        
        PreparedStatement pstmt = con.prepareStatement("Select display_name from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        while(rs.next())
        {
            if(tokens[1].equalsIgnoreCase(rs.getString(1)))
            {
                msg = "exists\n";
                outputStream.write(msg.getBytes());
                outputStream.flush();
                return;
            }
        }
        rs.close();
        pstmt.close();
        con.close();
        msg = "ok\n";
        outputStream.write(msg.getBytes());
        outputStream.flush();
        update_display_name(tokens);
    }

    private void update_display_name(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Update user_acc Set display_name=? where username=?");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.execute();
        
        pstmt.close();
        con.close();
    }

    private void change_password(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Update user_acc Set password=? where username=?");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.execute();
        
        pstmt.close();
        con.close();
    }

    private void change_basic(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Update user_acc Set first_name=?,last_name = ? where username=?");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.setString(3,tokens[3]);
        
        pstmt.execute();
        
        pstmt.close();
        con.close();
    }

    private String search_friends(String[] tokens) throws SQLException, IOException {
        connection();
        String response = "";
        ArrayList<Integer> posib_friends = new ArrayList<>();
        ArrayList<Integer> friends = new ArrayList<>();
        
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where Display_name Like '%"+tokens[1]+"%' and not id=? and not account_level =1 ");
        //pstmt.setString(1,tokens[1]+ "%");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            posib_friends.add(rs.getInt(1));
        }
        
        pstmt = con.prepareStatement("Select user_id1,user_id2 from friend_relationships where user_id1= ? or user_id2 = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        rs= pstmt.executeQuery();
        
         while(rs.next())
        {
            if(rs.getInt(1)==Integer.parseInt(tokens[2]))
            {
                friends.add(rs.getInt(2));
            }
            else
            {
                friends.add(rs.getInt(1));
            }
        }
      
        
        for(int x  = 0;x < posib_friends.size();x++)
        {
            if(!friends.contains(posib_friends.get(x))){
                pstmt = con.prepareStatement("Select display_name from user_acc where id =?");
                pstmt.setInt(1,posib_friends.get(x));
                rs = pstmt.executeQuery();
                response += rs.getString(1)+" ";
            }
            
        }
        
        response +="\n";
        rs.close();
        pstmt.close();
        con.close();
        return response;
    }

    private boolean add_friends(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        int id =-1;
        while(rs.next())
        {
            id  =rs.getInt(1);
            
        }
        if(id != -1)
        {
            pstmt = con.prepareStatement("Insert into friend_relationships(user_id1,user_id2,Approved) Values(?,?,?)");
            pstmt.setInt(1,Integer.parseInt(tokens[2]));
            pstmt.setInt(2,id);
            pstmt.setInt(3,3);
            pstmt.executeUpdate();
            
            pstmt = con.prepareStatement("Insert into friend_relationships(user_id1,user_id2,Approved) Values(?,?,?)");
            pstmt.setInt(1,id);
            pstmt.setInt(2,Integer.parseInt(tokens[2]));
            pstmt.setInt(3,0);
            pstmt.executeUpdate();
            
            
            rs.close();
            pstmt.close();
            con.close();
            return true;
        }
        else
        {
            rs.close();
            pstmt.close();
            con.close();
            return false;
        }
    }

    private String get_pending_invite(String[] tokens) throws SQLException, IOException {
        connection();
        ArrayList<Integer> id_list = new ArrayList<>();
        PreparedStatement pstmt = con.prepareStatement("Select user_id2 from friend_relationships where user_id1= ? and approved = 0");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        rs= pstmt.executeQuery();
        while(rs.next())
        {
            id_list.add(rs.getInt(1));
        }
        String response = "";
        for(int x =0;x < id_list.size();x++)
        {
            pstmt = con.prepareStatement("Select display_name from user_acc where id = ?");
            pstmt.setInt(1,id_list.get(x));
            rs= pstmt.executeQuery();
            while(rs.next())
            {
                response +=rs.getString(1)+" ";
            }
        }
        response += "\n";
        pstmt.close();
        rs.close();
        con.close();
        return response;
    }

    private void accept_friend(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        int id = 0;
        while(rs.next())
        {
            id = rs.getInt(1);
        }
        pstmt = con.prepareStatement("UPDATE friend_relationships set Approved=1 where user_id1 = ? and user_id2 = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        pstmt.setInt(2,id);
        pstmt.executeUpdate();
            
        pstmt = con.prepareStatement("UPDATE friend_relationships set approved=1 where user_id1 = ? and user_id2 = ?");
        pstmt.setInt(1,id);
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        pstmt.executeUpdate();
        
        pstmt.close();
        rs.close();
        con.close();
    }

    private void deny_friend(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        int id = 0;
        while(rs.next())
        {
            id = rs.getInt(1);
        }
        pstmt = con.prepareStatement("UPDATE friend_relationships set Approved=2 where user_id1 = ? and user_id2 = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        pstmt.setInt(2,id);
        pstmt.executeUpdate();
        
        pstmt.close();
        rs.close();
        con.close();
    }

    private String get_friend_list(String[] tokens) throws SQLException {
        connection();
        ArrayList<Integer> id_list = new ArrayList<>();
        
        PreparedStatement pstmt = con.prepareStatement("Select user_id2 from friend_relationships where user_id1 = ? and approved=1");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        rs = pstmt.executeQuery();
        
        String response = "";
        while(rs.next())
        {
            id_list.add(rs.getInt(1));
        }
        for(int x =0;x < id_list.size();x++)
        {
            pstmt = con.prepareStatement("Select display_name from user_acc where id = ?");
            pstmt.setInt(1,id_list.get(x));
            rs= pstmt.executeQuery();
            while(rs.next())
            {
                response +=rs.getString(1)+" ";
            }
        }
        response += "\n";
        pstmt.close();
        rs.close();
        con.close();
        
        return response;
    }

    private void unfriend_friend(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        int id = 0;
        while(rs.next())
        {
            id = rs.getInt(1);
        }
        pstmt = con.prepareStatement("DELETE FROM friend_relationships where user_id1 = ? and user_id2 = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        pstmt.setInt(2,id);
        pstmt.executeUpdate();
            
        pstmt = con.prepareStatement("DELETE FROM friend_relationships where user_id1 = ? and user_id2 = ?");
        pstmt.setInt(1,id);
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        pstmt.executeUpdate();
        
        pstmt.close();
        rs.close();
        con.close();
    }

    private void is_duplicate(String[] tokens) throws SQLException {
        connection();
        msg  = "ok\n";
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[2]);
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            msg = "exist Display_name\n";
            return;
        }
        
        pstmt = con.prepareStatement("Select id from user_acc where email = ?");
        pstmt.setString(1,tokens[3]);
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            msg = "exist email\n";
            return;
        }
        pstmt = con.prepareStatement("Select id from user_acc where username =?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            msg = "exist username\n";
            return;
        }
        rs.close();
        pstmt.close();
        con.close();
            
    }

    private void register_user(String[] tokens) throws SQLException {
        connection();
        msg  = "ok\n";
        PreparedStatement pstmt = con.prepareStatement("INSERT INTO user_acc(First_name,Last_name,Username,Display_name,password,email) Values(?,?,?,?,?,?)");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.setString(3,tokens[3]);
        pstmt.setString(4,tokens[4]);
        pstmt.setString(5,tokens[5]);
        pstmt.setString(6,tokens[6]);
        pstmt.executeUpdate();
    }

    private void user_complaint(String line) throws SQLException {
        String tokens[] = StringUtils.split(line,null,3);
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where username = ? or display_name = ? or email = ?");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[1]);
        pstmt.setString(3,tokens[1]);
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            PreparedStatement pstmt2 =con.prepareStatement("Insert into Passwd_reset(user_iden,text) Values(?,?)");
            pstmt2.setString(1,tokens[1]);
            pstmt2.setString(2,tokens[2]);
            pstmt2.executeUpdate();
            pstmt2.close();
            rs.close();
            con.close();
            return;
        }
        pstmt.close();
        rs.close();
        con.close();
        return;
    }

    private String get_user_list() throws SQLException {
        connection();
        PreparedStatement pstmt =con.prepareStatement("Select username from user_acc where Account_level = 0");
        rs = pstmt.executeQuery();
        String  response =""; 
        while(rs.next())
        {
            response += rs.getString(1)+" ";
        }
        response +="\n";
        rs.close();
        pstmt.close();
        con.close();
        return response;
    }

    private String block_unblock_user(String[] tokens) throws SQLException {
        connection();
        rs = st.executeQuery("Select username,email,ISblocked from User_acc");
        while(rs.next())
        {
            if(rs.getString(1).equals(tokens[1])||rs.getString(2).equals(tokens[1]))
            {
               if(rs.getBoolean(3) == false)
                {
                    PreparedStatement pstmt = con.prepareStatement("Update user_acc Set isblocked = true Where username = ? OR email = ?");
                    pstmt.setString(1,tokens[1]);
                    pstmt.setString(2,tokens[1]);
                    pstmt.executeUpdate();
                    rs.close();
                    pstmt.close();
                    con.close();
                    return "Blocked\n";        
                }
                else
                {
                    PreparedStatement pstmt = con.prepareStatement("Update user_acc Set isblocked = false Where username = ? OR email = ?");
                    pstmt.setString(1,tokens[1]);
                    pstmt.setString(2,tokens[1]);
                    pstmt.executeUpdate();
                    rs.close();
                    pstmt.close();
                    con.close();    
                    return "Unblocked\n";
                }
            }            
        }
        rs.close();
        con.close();
        return "No_results\n";
    }

    private void delete_user(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Delete from user_acc where username=?");
        pstmt.setString(1,tokens[1]);
        pstmt.executeUpdate();
        rs.close();
        pstmt.close();
        con.close();
    }

    private String search_user(String[] tokens) throws SQLException {
        connection();
        rs = st.executeQuery("Select username from User_acc where (username Like '%"+tokens[1]+"%' or email Like '%"+tokens[1]+"%') and account_level = 0");
        String response ="";
        while(rs.next())
        {
            response += rs.getString(1)+" ";
        }
        response += "\n";
        rs.close();
        con.close();
        if(response.equals("\n"))
            return "no_results\n";
        else
            return response;
    }

    private void get_user_log(String[] tokens) throws SQLException, IOException {
        // ORDER BY Date ASC
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where username=?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        int id = 0;
        while(rs.next())
        {
            id = rs.getInt(1);
        }
        
        pstmt = con.prepareStatement("Select Text from Log where userid=?");
        pstmt.setInt(1,id);
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            msg = rs.getString(1)+"\n";
            outputStream.write(msg.getBytes());
            outputStream.flush();
      
        }
        msg = "end\n";
        outputStream.write(msg.getBytes());
        outputStream.flush();
        rs.close();
        pstmt.close();
        con.close();
    }

    private void register_user_ad(String[] tokens) throws SQLException {
        connection();
        msg  = "ok\n";
        PreparedStatement pstmt = con.prepareStatement("INSERT INTO user_acc(First_name,Last_name,Username,Display_name,password,email,account_level) Values(?,?,?,?,?,?,?)");
        pstmt.setString(1,tokens[1]);
        pstmt.setString(2,tokens[2]);
        pstmt.setString(3,tokens[3]);
        pstmt.setString(4,tokens[4]);
        pstmt.setString(5,tokens[5]);
        pstmt.setString(6,tokens[6]);
        pstmt.setInt(7,Integer.parseInt(tokens[7]));
        pstmt.executeUpdate();
        
        pstmt.close();
        con.close();
    }

    private void update_password_from_ad(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Update user_acc Set password=? where username=?");
        pstmt.setString(1,tokens[2]);
        pstmt.setString(2,tokens[1]);
        pstmt.execute();
        
        pstmt.close();
        con.close();
    }

    private String get_group_info(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select title,creator,id,desc from groups where title = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        String response = "";
        
        while(rs.next())
        {
            response += rs.getString(1)+" "+rs.getInt(2)+" "+rs.getInt(3)+" "+rs.getString(4);                                                   
        }
        response +="\n";
        rs.close();
        pstmt.close();
        con.close();
        return response;
    }

    private void leave_group(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Delete from chat_members where user_id =? and chat_id =?");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        
        pstmt.executeUpdate();
        rs.close();
        pstmt.close();
        con.close();
    }

    private void delete_group(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Delete from chat_members where chat_id =?");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        
        pstmt.executeUpdate();
        
        pstmt = con.prepareStatement("Delete from Groups where id =?");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        
        pstmt.executeUpdate();
        
        
        rs.close();
        pstmt.close();
        con.close();
    }

    private void member_of_this_group(String[] tokens) throws SQLException, IOException {
        connection();
        ArrayList<Integer> id = new ArrayList<>();
        
        PreparedStatement pstmt = con.prepareStatement("Select user_id from chat_members where chat_id = ? and not user_id=?");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            id.add(rs.getInt(1));
        }        
 
        String response ="";
        for(int ids: id)
        {
             pstmt = con.prepareStatement("Select display_name from user_acc where id = ? ");
             pstmt.setInt(1,ids);
             rs = pstmt.executeQuery();
             
             response +=rs.getString(1)+" ";
        }

        
        response += "\n";
        outputStream.write(response.getBytes());
        
        pstmt.close();
        rs.close();
        con.close();
    }

    private void remove_member(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        
        int id = rs.getInt(1);
        
        pstmt = con.prepareStatement("Delete From chat_members where user_id = ?");
        pstmt.setInt(1,id);
        
        pstmt.executeUpdate();
        pstmt.close();
        rs.close();
        con.close();
    }

    private void get_add_friend_member_list(String[] tokens) throws SQLException, IOException {
        connection();
        ArrayList<Integer> id = new ArrayList<>();
        
        PreparedStatement pstmt = con.prepareStatement("Select user_id2 from friend_relationships where user_id1 = ? and approved = 1");
        pstmt.setInt(1,Integer.parseInt(tokens[1]));
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            id.add(rs.getInt(1));
        }        
 
        pstmt = con.prepareStatement("Select user_id from chat_members where chat_id = ?");
        pstmt.setInt(1,Integer.parseInt(tokens[2]));
        rs = pstmt.executeQuery();
        
        while(rs.next())
        {
            if(id.contains(rs.getInt(1)))
            {
                id.remove(Integer.valueOf(rs.getInt(1)));
            }
        }
        String response = "";
        
        for(int ids: id)
        {
             pstmt = con.prepareStatement("Select display_name from user_acc where id = ? ");
             pstmt.setInt(1,ids);
             rs = pstmt.executeQuery();
             
             response +=rs.getString(1)+" ";
        }
        
        response += "\n";
        outputStream.write(response.getBytes());
        
        pstmt.close();
        rs.close();
        con.close();
        
    }

    private void add_friend_to_group(String[] tokens) throws SQLException {
        connection();
        PreparedStatement pstmt = con.prepareStatement("Select id from user_acc where display_name = ?");
        pstmt.setString(1,tokens[1]);
        rs = pstmt.executeQuery();
        
        int id = rs.getInt(1);
        
        pstmt = con.prepareStatement("Insert into chat_members(user_id,chat_id) values(?,?)");
        pstmt.setInt(1,id);
        pstmt.setInt(2,Integer.parseInt(tokens[2]));
        
        pstmt.executeUpdate();
        pstmt.close();
        rs.close();
        con.close();
    }

}
